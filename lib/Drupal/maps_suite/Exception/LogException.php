<?php

/**
 * @file
 * Define the MaPS Suite Log Exception class.
 */

namespace Drupal\maps_suite\Exception;

/**
 * Maps Suite Log Exception.
 */
class LogException extends Exception {}
