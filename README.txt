README.txt
==========

This module provides a bridge with MaPS System® software. MaPS Suite is a tool for managing large data and performing multichannel publications.

Data from MaPS System® are imported into entities and fields, using an intuitive User Interface, that exposes the following features:

* MaPS System® profile creation/edition/removal
* configuration management (languages, statuses, media directories)
* validation of the field correspondence
* conversion of the MaPS System® libraries to taxonomy
* administration of the correspondences between MaPS System® objects and Drupal entities
* setting-up of advanced filters for each converter
* management of the fields and properties mapping for each correspondence
* manual and automatic import