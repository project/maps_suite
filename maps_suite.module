<?php

/**
 * @file
 * The MaPS Suite module exposes some helpers for other modules.
 */

/**
 * @defgroup MaPS Suite constants
 * @{
 * The order of the words in the constants names is not
 * the expected natural one.
 * The purpose here is to help the IDE, which autocomplete
 * the constant name when typing,
 * to display really helpfull suggestions.
 * That's why the most generic words should
 * appear first in the constant name.
 *
 * In order to identify properly the entity the constant
 * refers to, a correctly formattedname is required in
 * the comments above the constant definition.
 */

/**
 * MaPS Suite context for translations.
 */
define('MAPS_SUITE_CONTEXT_MAPS', 'maps_suite');

/**
 * MaPS System® default language ID
 */
define('MAPS_SUITE_MAPS_SYSTEM_LANGUAGE_DEFAULT_ID', 1);

/**
 * MaPS Suite textgroup for locale translations.
 */
define('MAPS_SUITE_LOCALE_TEXTGROUP', 'maps_suite');

/**
 * MaPS System® allowed attributes types
 */
define('MAPS_SUITE_MAPS_SYSTEM_ALLOWED_ATTRIBUTE_TYPES', 'bool|string|int|text|date|float|library|html|object');

/**
 * @} End of "defgroup MaPS Suite constants".
 *
 * @defgroup Drupal hooks
 * @{
 */

/**
 * Implements hook_menu().
 */
function maps_suite_menu() {
  $items = array();

  $items['admin/maps-suite'] = array(
    'title' => 'MaPS Suite',
    'description' => 'MaPS Suite Suite overview.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('administer maps suite'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/maps-suite/log.xsl'] = array(
    'title' => 'MaPS Suite Log XSL',
    'page callback' => 'maps_suite_log_xsl_page',
    'access arguments' => array('administer maps suite'),
    'delivery callback' => 'maps_suite_deliver_xsl_file',
    'type' => MENU_CALLBACK,
  );

  $items['admin/maps-suite/settings'] = array(
    'title' => 'Settings',
    'description' => 'Administer MaPS Suite: Web Services profiles, import configuration, entities mapping, etc.',
    'position' => 'left',
    'page callback' => 'maps_suite_admin_overview',
    'access arguments' => array('administer maps suite'),
    'weight' => -10,
    'file' => 'maps_suite.admin.inc',
  );

  $items['admin/maps-suite/settings/overview'] = array(
    'title' => 'Overview',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/maps-suite/settings/log'] = array(
    'title' => 'Log settings',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('maps_suite_admin_log_form'),
    'access arguments' => array('administer maps suite'),
    'file' => 'maps_suite.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function maps_suite_permission() {
  return array(
    'administer maps suite' => array(
      'title' => t('Administer MaPS Suite'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function maps_suite_theme() {
  return array(
    'maps_suite_log_xsl' => array(
      'path' => drupal_get_path('module', 'maps_suite') . '/theme',
      'render element' => 'page',
      'template' => 'maps-suite-log-xsl',
    ),
  );
}

/**
 * Implements hook_hook_info().
 */
function maps_suite_hook_info() {
  return array(
    'maps_suite_admin_overview' => array(
      'group' => 'maps_suite',
    ),
  );
}

/**
 * Implements hook_element_info_alter().
 */
function maps_suite_element_info_alter(&$type) {
  $type['checkboxes']['#process'][] = 'maps_suite_process_checkboxes';
}

/**
 * Implements hook_flush_caches().
 */
function maps_suite_flush_caches() {
  return array('cache_maps_suite');
}

/**
 * @} End of "defgroup Drupal hooks".
 *
 * @defgroup Admin menu
 * @{
 */

/**
 * Implements hook_admin_menu_cache_info().
 */
function maps_suite_admin_menu_cache_info() {
  return array(
    'cache_maps_suite' => array(
      'title' => t('MaPS Suite'),
      'callback' => 'maps_suite_flush_cache',
    ),
  );
}

/**
 * Admin menu callback; Flush the {cache_maps_suite} table.
 */
function maps_suite_flush_cache() {
  cache_clear_all('*', 'cache_maps_suite', TRUE);
}

/**
 * @} End of "defgroup Admin menu".
 */

/**
 * Menu callback; display the Log XSL file.
 */
function maps_suite_log_xsl_page() {
  $path = drupal_get_path('module', 'maps_suite');

  drupal_set_title(t('Log report'));
  drupal_add_css($path . '/css/maps-suite-log-xsl.css');
  drupal_add_js($path . '/js/mapsSuiteLogXsl.js');

  return '<xsl:apply-templates/>';
}

/**
 * Process variables for maps-suite-log-xsl.tpl.php.
 */
function template_process_maps_suite_log_xsl(&$variables) {
  // We need to remove the doctype tag if exists.
  $variables['page'] = preg_replace('/<!DOCTYPE[^>]*>/i', '', $variables['page']['#children']);
}

/**
 * Delivery callback for XSL files.
 */
function maps_suite_deliver_xsl_file($page_callback_result) {
  if (isset($page_callback_result) && is_null(drupal_get_http_header('Content-Type'))) {
    drupal_add_http_header('Content-type', 'text/xml');
  }

  drupal_set_page_content($page_callback_result);
  $xsl = element_info('page');
  $xsl['#theme_wrappers'][] = 'maps_suite_log_xsl';

  print drupal_render_page($xsl);
  drupal_page_footer();
}

/**
 * Processes a checkboxes form element.
 *
 * Allow to disable some options inside a group of checkboxes.
 */
function maps_suite_process_checkboxes($element) {
  if (!empty($element['#disabled_options'])) {
    foreach ($element['#disabled_options'] as $key) {
      $element[$key]['#disabled'] = TRUE;
    }
  }

  return $element;
}

/**
 * Helper that automatically adds context for translation.
 *
 * This avoid conflicts with existing translations for strings that
 * may have several interpretations.
 * To translate strings belonging to the global (empty) context, use
 * t() function.
 *
 * @see t()
 * @see maps_suite_format_string()
 */
function maps_suite_t($string, array $args = array(), $context = MAPS_SUITE_CONTEXT_MAPS, array $options = array()) {
  $options['context'] = $context;
  maps_suite_format_string($string, $args);
  return t($string, $args, $options);
}

/**
 * Replaces extra placeholders with processed values in a string.
 *
 * @param string $string
 *   A string containing placeholders.
 * @param array $args
 *   An array of $arguments. Key is used for text replacement and value may
 *   be processed in different ways:
 *   - key begins by $: The PHP representation of the value variable is wrapped
 *     inside two HR HTML tags.
 *   - key begins by %, @ or !: see format_string().
 *
 * @see format_string()
 */
function maps_suite_format_string(&$string, array &$args) {
  foreach ($args as $key => $value) {
    if ($key[0] === '$') {
      unset($args[$key]);
      $new_key = '!' . drupal_substr($key, 1);
      $string = strtr($string, array($key => $new_key));
      $args[$new_key] = maps_suite_highlight_php($value);
    }
  }
}

/**
 * Displays a PHP variable using colors and indentation.
 */
function maps_suite_highlight_php($variable) {
  return highlight_string("<?php\n" . var_export($variable, TRUE), TRUE);
}

/**
 * Translate user defined string.
 *
 * This function relies on i18n String module if installed.
 *
 * @param string $string
 *   The string to translate
 * @param array $options
 *   Options array. @see i18n_string().
 */
function maps_suite_user_t($name, $string, $langcode = NULL, $textgroup = MAPS_SUITE_LOCALE_TEXTGROUP, array $options = array()) {
  // $options['context'] = $context;
  return function_exists('i18n_string') ? i18n_string("$textgroup:$name", $string, array('langcode' => $langcode)) : $string;
}

/**
 * FAPI; validation of an individual integer element.
 *
 * The form element may contain some extra keys:
 * - #positive: 0 means no extra validation, 1 implies that expected value
 *   is a positive integer and -1 a negative one.
 * - #not_null: A boolean indicating if the expected value may be equal to
 *   zero or not.
 * - #min: The minimum value.
 * - #max: The maximum value.
 */
function maps_suite_integer_element_validate($element, &$form_state) {
  $value = trim($element['#value'], " \t\n\r\x0B");

  if (isset($element['#value']) && strlen($value)) {
    $value = $value == 0 ? '0' : ltrim($value, '0');
    $regex = '[0-9]+';
    $in_error = FALSE;

    $limits = array();
    foreach (array('min', 'max') as $key) {
      if (isset($element["#$key"]) && is_numeric($element["#$key"])) {
        $limits[$key] = $element["#$key"];
      }
    }

    // Build regular expression and error message.
    $message = array();

    if (!empty($element['#not_null'])) {
      $message[] = 'not null';
      $regex = '[1-9][0-9]*';

      if (!isset($article)) {
        $article = 'a';
      }
    }
    if (!empty($element['#positive']) && abs($element['#positive']) === 1) {
      if (!isset($article)) {
        $article = 'a';
      }

      if ($element['#positive'] > 0) {
        $message[] = 'positive';
      }
      else {
        $message[] = 'negative';
        $regex = '-' . $regex;
      }
    }

    array_unshift($message, 'Field %field: value should be ' . (isset($article) ? $article : 'an'));
    $message[] = 'integer.';
    $t_args = array('%field' => isset($element['#title']) ? $element['#title'] : array_pop($element['#parents']));

    // Basic validation.
    if (!preg_match('~^' . $regex . '$~', $value)) {
      $in_error = TRUE;
    }
    // Check for limits.
    elseif ((isset($limits['min']) && $value < $limits['min']) || (isset($limits['max']) && $value > $limits['max'])) {
      $in_error = TRUE;

      if (isset($limits['min'])) {
        $message[] = 'greather than %min';
        $t_args['%min'] = $limits['min'];
      }
      if (isset($limits['max'])) {
        if (isset($limits['min'])) {
          $message[] = 'and';
        }

        $message[] = 'lower than %max';
        $t_args['%max'] = $limits['max'];
      }
    }

    if ($in_error) {
      form_error($element, t(implode(' ', $message), $t_args));
    }
    elseif (strlen($value) != strlen($element['#value'])) {
      form_set_value($element, $value, $form_state);
    }
  }
}

/**
 * Retrieves database entries for the specified table.
 *
 * @param string $table
 *   The database table.
 * @param string $key
 *   The column used as key for the results array.
 * @param array $conditions
 *   An associative array of conditions on the base table, where the
 *   keys are the database fields and the values are the values those
 *   fields must have.
 * @param string $fetch
 *   The fetchmode to use.
 */
function maps_suite_get_records($table, $key = NULL, array $conditions = array(), $fetch = NULL) {
  if ($schema = drupal_get_schema($table)) {
    $match = array();
    preg_match_all('/(?:^|_)([\w]{1})/i', $table, $match);
    $alias = implode('', $match[1]);

    $serialized = array();
    foreach ($schema['fields'] as $column => $info) {
      if (!empty($info['serialize'])) {
        $serialized[] = $column;
      }
    }

    $query = db_select($table, $alias);
    $query->fields($alias);

    if (array_key_exists('weight', $schema['fields'])) {
      $query->orderBy("$alias.weight");
    }

    if ($key) {
      $query->orderBy("$alias.$key");
    }

    foreach ($conditions as $field => $value) {
      $query->condition("$alias.$field", $value);
    }

    $result = $query->execute();

    if (isset($fetch)) {
      if (is_string($fetch)) {
        $result->setFetchMode(\PDO::FETCH_CLASS, $fetch);
      }
      else {
        $result->setFetchMode($fetch);
      }
    }

    if (!$serialized) {
      return $key ? $result->fetchAllAssoc($key) : $result->fetchAll();
    }

    $records = array();

    foreach ($result->fetchAll() as $record) {
      foreach ($serialized as $column) {
        if (is_object($record)) {
          if (isset($record->$column)) {
            $record->$column = @unserialize($record->$column);
          }
        }
        else {
          if (isset($record[$column])) {
            $record[$column] = @unserialize($record[$column]);
          }
        }
      }

      if ($key) {
        $k = is_object($record) ? $record->$key : $record[$key];
        $records[$k] = $record;
      }
      else {
        $records[] = $record;
      }
    }

    return $records;
  }

  return FALSE;
}

/**
 * Reduces a multidimentional array to a simple 1-level array.
 */
function maps_suite_reduce_array($array, $key, $is_object = FALSE) {
  $target = '$v' . ($is_object ? '->' . $key : '["' . $key . '"]');
  $callback = create_function('$v', "return isset($target) ? $target : NULL;");
  return array_map($callback, $array);
}

/**
 * Convert a string from Drupal style to Camel case.
 */
function maps_suite_drupal2camelcase($string, $start_uppercase = TRUE) {
  $pattern = $start_uppercase ? '(?:^|_)' : '_';
  $callback = function($match) {
    return strtoupper($match[1]);
  };
  return preg_replace_callback('/' . $pattern . '(\w)/', $callback, strtolower($string));
}
