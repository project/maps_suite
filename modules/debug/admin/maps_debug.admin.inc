<?php

use Drupal\maps_import\Cache\Object\Converter as CacheConverter;
use Drupal\maps_import\Converter\ConverterInterface;
use Drupal\maps_import\Operation\Entity\Delete\Delete;
use Drupal\maps_import\Profile\Profile;
use Drupal\maps_import\Tools\Object\MapsObjects;

/**
 * Form builder; profile force reimport form.
 */
function maps_debug_profile_force_reimport_form($form, &$form_state, Profile $profile) {
  $form['#profile'] = $profile;

  return confirm_form(
    $form,
    t('Are you sure you want to force entities reimport for profile @profile ?', array('@profile' => $profile->getTitle())),
    'admin/maps-suite/profiles',
    '',
    t('Force reimport'),
    t('Cancel')
  );
}

/**
 * Form submit.
 * Force reimport for Profile entities.
 */
function maps_debug_profile_force_reimport_form_submit($form, &$form_state) {
  $mapsObjects = new MapsObjects();
  $mapsObjects->setProfileObjectsAsInserted($form['#profile']);

  drupal_set_message(t('Force reimport for profile %profile', array('%profile' => $form['#profile']->getName())));
  $form_state['redirect'] = array('admin/maps-suite/profiles');
}

/**
 * Form builder; converter force reimport form.
 */
function maps_debug_converter_force_reimport_form($form, &$form_state, Profile $profile, ConverterInterface $converter) {
  $form['#converter'] = $converter;

  return confirm_form(
    $form,
    t('Are you sure you want to force entities reimport for converter @converter ', array('@converter' => $converter->getTitle())),
    'admin/maps-suite/profiles/' . $profile->getName() . '/' . $converter->getType(),
    '',
    t('Force reimport'),
    t('Cancel')
  );
}

/**
 * Form submit.
 * Force reimport for Profile entities.
 */
function maps_debug_converter_force_reimport_form_submit($form, &$form_state) {
  $mapsObjects = new MapsObjects();
  $mapsObjects->setConverterObjectsAsInserted($form['#converter']);

  drupal_set_message(t('Force reimport for converter %converter', array('%converter' => $form['#converter']->getName())));
  $form_state['redirect'] = array('admin/maps-suite/profiles/' . $form['#converter']->getProfile()->getName() . '/' . $form['#converter']->getType());
}


/**
 * Form builder; profile delete entities form.
 */
function maps_debug_profile_delete_entities_form($form, &$form_state, Profile $profile) {
  $form['#profile'] = $profile;

  return confirm_form(
    $form,
    t('Are you sure you want to delete entities for profile @profile ?', array('@profile' => $profile->getTitle())),
    'admin/maps-suite/profiles',
    '',
    t('Delete entities'),
    t('Cancel')
  );
}

/**
 * Form submit.
 * Delete entities for Profile.
 */
function maps_debug_profile_delete_entities_form_submit($form, &$form_state) {
  // @todo
  $profile = $form['#profile'];
  foreach (CacheConverter::getInstance()->load(array($profile->getPid()), 'pid') as $cid => $converter) {
    $delete = new Delete($converter);
    $delete->process();
  }

  drupal_set_message(t('Delete entities for profile %profile', array('%profile' => $form['#profile']->getName())));
  $form_state['redirect'] = array('admin/maps-suite/profiles');
}

/**
 * Form builder; converter delete entities form.
 */
function maps_debug_converter_delete_entities_form($form, &$form_state, Profile $profile, ConverterInterface $converter) {
  $form['#converter'] = $converter;

  return confirm_form(
    $form,
    t('Are you sure you want to delete entities for converter @converter ', array('@converter' => $converter->getTitle())),
    'admin/maps-suite/profiles/' . $profile->getName() . '/' . $converter->getType(),
    '',
    t('Delete entities'),
    t('Cancel')
  );
}

/**
 * Form submit.
 * Delete entities for converter.
 */
function maps_debug_converter_delete_entities_form_submit($form, &$form_state) {
  $delete = new Delete($form['#converter']);
  $delete->process();

  drupal_set_message(t('Delete entities for converter %converter', array('%converter' => $form['#converter']->getName())));
  $form_state['redirect'] = array('admin/maps-suite/profiles/' . $form['#converter']->getProfile()->getName() . '/' . $form['#converter']->getType());
}
