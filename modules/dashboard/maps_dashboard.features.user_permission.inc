<?php
/**
 * @file
 * maps_dashboard.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function maps_dashboard_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access maps dashboard'.
  $permissions['access maps dashboard'] = array(
    'name' => 'access maps dashboard',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'maps_dashboard',
  );

  return $permissions;
}
