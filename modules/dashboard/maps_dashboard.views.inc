<?php

/**
 * Implements hook_views_data().
 */
function maps_dashboard_views_data() {
  $data['maps_import_object_media'] = array(
    'table' => array(
      'group' => t('MaPS Import Object/Media'),
      'join' => array(
        'maps_import_objects' => array(
          'left_field' => 'id',
          'field' => 'object_id',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\ObjectMedia',
        ),
        'maps_import_medias' => array(
          'left_field' => 'id',
          'field' => 'media_id',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\ObjectMedia',
        ),
      ),
    ),
    // Object id.
    'object_id' => array(
      'title' => t('Object id'),
      'help' => t('The MaPS Object id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Media id.
    'media_id' => array(
      'title' => t('Media id'),
      'help' => t('The MaPS Media id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
  );

  $data['maps_import_entities'] = array(
    'table' => array(
      'group' => t('MaPS Import Entities'),
      'base' => array(
        'field' => 'id',
        'title' => t('MaPS Import Entities'),
        'help' => t('The imported Drupal entities.'),
      ),
      'join' => array(
        'maps_import_objects' => array(
          'left_field' => 'pid',
          'field' => 'pid',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\Object2Entity',
          'pivot_table' => 'maps_import_object_ids',
          'type' => 'LEFT',
        ),
        'maps_import_medias' => array(
          'left_field' => 'pid',
          'field' => 'pid',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\Object2Entity',
          'pivot_table' => 'maps_import_media_ids',
          'type' => 'LEFT',
        ),
      ),
    ),
    // Id.
    'id' => array(
      'title' => t('Entity ids'),
      'help' => t('The mapping between Drupal and MaPS'),
    ),
    // Profile id.
    'pid' => array(
      'title' => t('Profile'),
      'help' => t('The related profile.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Profile',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Profile',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Converter id.
    'cid' => array(
      'title' => t('Converter'),
      'help' => t('The related converter.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Converter',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Converter',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Entity id.
    'entity_id' => array(
      'title' => t('Entity id'),
      'help' => t('The Drupal entity id.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\EntityId',
        'click sortable' => TRUE,
        'additional fields' => array(
          'entity_type' => 'entity_type',
        ),
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Entity type.
    'entity_type' => array(
      'title' => t('Entity type'),
      'help' => t('The Drupal entity type.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\EntityType',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Bundle.
    'bundle' => array(
      'title' => t('Bundle'),
      'help' => t('The Drupal bundle.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Bundle',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Uid.
    'uid' => array(
      'title' => t('Uid'),
      'help' => t('The unique identifier.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Uid scope.
    'uid_scope' => array(
      'title' => t('Uid scope'),
      'help' => t('The unique identifier scope.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\UidScope',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Id language.
    'id_language' => array(
      'title' => t('Language id'),
      'help' => t('The entity language id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
  );

  $data['maps_import_objects'] = array(
    'table' => array(
      'group' => t('MaPS Import Objects'),
      'base' => array(
        'title' => t('MaPS Import Objects'),
        'help' => t(''),
        'field' => 'id',
      ),
      'join' => array(
        'maps_import_entities' => array(
          'left_field' => 'pid',
          'field' => 'pid',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\Object2Entity',
          'pivot_table' => 'maps_import_object_ids',
          'type' => 'LEFT',
        ),
        'maps_import_object_media' => array(
          'left_field' => 'object_id',
          'field' => 'id',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\ObjectMedia',
        ),
      ),
    ),
    // ID.
    'id' => array(
      'title' => t('Object id'),
      'help' => t('The mapping between Drupal and MaPS'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\ObjectId',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Parent id.
    'parent_id' => array(
      'title' => t('Parent object id'),
      'help' => t('The parent object id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Source id.
    'source_id' => array(
      'title' => t('Source object id'),
      'help' => t('The source object id.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Sequence.
    'weight' => array(
      'title' => t('Sequence'),
      'help' => t('The object sequence.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Profile id.
    'pid' => array(
      'title' => t('Profile'),
      'help' => t('The related profile.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Profile',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Profile',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Updated.
    'updated' => array(
      'title' => t('Updated'),
      'help' => t('The updated date timestamp.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Inserted.
    'inserted' => array(
      'title' => t('Inserted'),
      'help' => t('The inserted date timestamp.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Config.
    'config_type' => array(
      'title' => t('Config'),
      'help' => t('The configuration type'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\ConfigurationType',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\ConfigurationType',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Code.
    'code' => array(
      'title' => t('Object code'),
      'help' => t('The MaPS Object code.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Status.
    'status' => array(
      'title' => t('Status'),
      'help' => t('The object status.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Status',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Status',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Nature.
    'nature' => array(
      'title' => t('Object nature'),
      'help' => t('The object nature.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Nature',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Type.
    'type' => array(
      'title' => t('Object type'),
      'help' => t('The object type.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Type',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Attributes.
    'attributes' => array(
      'title' => t('Attributes'),
      'help' => t('The MaPS Object attributes.'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
  );

  $data['maps_import_medias'] = array(
    'table' => array(
      'group' => t('MaPS Import Medias'),
      'base' => array(
        'title' => t('MaPS Import Medias'),
        'help' => t(''),
        'field' => 'id',
      ),
      'join' => array(
        'maps_import_entities' => array(
          'left_field' => 'pid',
          'field' => 'pid',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\Object2Entity',
          'pivot_table' => 'maps_import_medias_ids',
          'type' => 'LEFT',
        ),
        'maps_import_object_media' => array(
          'left_field' => 'media_id',
          'field' => 'id',
          'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Join\\ObjectMedia',
        ),
      ),

    ),
    // ID.
    'id' => array(
      'title' => t('Media id'),
      'help' => t('The mapping between Drupal and MaPS'),
      'relationship' => array(
        'base' => 'maps_import_media_ids',
        'base field' => 'maps_id',
        'handler' => 'views_handler_relationship',
        'label' => t('Entity id'),
      ),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\MediaId',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Extension.
    'extension' => array(
      'title' => t('Extension'),
      'help' => t('The media extension'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Media type.
    'type' => array(
      'title' => t('Media type'),
      'help' => t('The MaPS System media type.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\MediaType',
        'click sortable' => TRUE,
        'additional fields' => array(
          'pid' => 'pid',
        ),
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    ),
    // Filename.
    'filename' => array(
      'title' => t('Filename'),
      'help' => t('The media filename'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Updated.
    'updated' => array(
      'title' => t('Updated'),
      'help' => t('The updated date timestamp.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
    // Inserted.
    'inserted' => array(
      'title' => t('Inserted'),
      'help' => t('The inserted date timestamp.'),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    ),
    // Url.
    'url' => array(
      'title' => t('Url'),
      'help' => t('The media url'),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_string',
      ),
    ),
    // Profile id.
    'pid' => array(
      'title' => t('Profile'),
      'help' => t('The related profile.'),
      'field' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Field\\Profile',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'Drupal\\maps_dashboard\\Views\\Handler\\Filter\\Profile',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
      'argument' => array(
        'handler' => 'views_handler_argument_numeric',
      ),
    ),
  );

  return $data;
}
