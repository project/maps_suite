<?php

/**
 * @file
 * Definition of the Status field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
abstract class Configuration extends Field {

  protected $configuration_type = false;

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($return = parent::render($values)) {
      return $return;
    }

    $value = $this->get_value($values);

    // Check in configuration to have the human readable label.
    if (!empty($this->configuration_type)) {
      $profile = $this->loadProfile($values);

      // Try to get the configuration for the current language.
      $configuration = $profile->getConfigurationTypes($this->configuration_type, $profile->getDefaultLanguage());

      // The MaPS System configuration may be incomplete, so we try for the default language.
      if (empty($configuration)) {
        $configuration = $profile->getConfigurationTypes($this->configuration_type, 0);
      }

      if (!empty($configuration[$value])) {
        $title = isset($configuration[$value]['title']) ? $configuration[$value]['title'] : $configuration[$value]['code'];
        return "$title ($value)";
      }
    }

    return $value;
  }

}
