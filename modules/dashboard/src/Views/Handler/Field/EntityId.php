<?php

/**
 * @file
 * Definition of the Profile field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
class EntityId extends \views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $value = $this->get_value($values);
    if (is_null($value)) {
      return FALSE;
    }

    $entity_type = $this->get_value($values, 'entity_type');

    // Load the entity to get its uri.
    $entities = entity_load($entity_type, array($value));
    $entity = reset($entities);

    $uri = entity_uri($entity_type, $entity);
    return l($value, $uri['path'], $uri['options']);
  }
}
