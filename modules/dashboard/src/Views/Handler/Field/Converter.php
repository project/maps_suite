<?php

/**
 * @file
 * Definition of the Converter field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

use Drupal\maps_import\Cache\Object\Converter as CacheConverter;

/**
 * Provides a custom views field.
 */
class Converter extends Field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($return = parent::render($values)) {
      return $return;
    }

    $cid = $this->get_value($values);
    if (empty($cid)) {
      return FALSE;
    }

    $converter = CacheConverter::getInstance()->loadSingle((int) $cid);

    return '<a href="/admin/maps-suite/profiles/' . $converter->getProfile()->getName() . '/' . $converter->getType() . '/' . $converter->getCid() . '">' . $converter->getTitle() . '</a>';
  }
}
