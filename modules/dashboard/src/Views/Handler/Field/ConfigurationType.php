<?php

/**
 * @file
 * Definition of the ConfigurationType field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

use Drupal\maps_import\Fetcher\Configuration as ConfigurationFetcher;

/**
 * Provides a custom views field.
 */
class ConfigurationType extends \views_handler_field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    $types = array(
      ConfigurationFetcher::ATTRIBUTE_OBJECT => t('Attribute object'),
      ConfigurationFetcher::CRITERIA_OBJECT => t('Criteria object'),
      ConfigurationFetcher::LINKED_OBJECT => t('Linked object'),
    );

    return isset($types[$this->get_value($values)]) ? $types[$this->get_value($values)] : '';
  }

}
