<?php

/**
 * @file
 * Definition of the Status field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
class Status extends Configuration {

  /**
   * {@inheritdoc}
   */
  protected $configuration_type = 'status';

}
