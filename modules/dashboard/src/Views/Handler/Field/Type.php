<?php

/**
 * @file
 * Definition of the Type field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
class Type extends Configuration {

  /**
   * {@inheritdoc}
   */
  protected $configuration_type = 'object_type';

}
