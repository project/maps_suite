<?php

/**
 * @file
 * Definition of the Profile field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
class MediaId extends \views_handler_field {

  public function option_definition() {
    $options = parent::option_definition();
    $options['link_to_dashboard'] = ['default' => TRUE];
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    $form['link_to_dashboard'] = array(
      '#type' => 'checkbox',
      '#title' => t('Link to the media dashboard'),
      '#description' => t('Link the id to the detail media dashboard.'),
      '#default_value' => $this->options['link_to_dashboard'],
    );
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if (empty($this->options['link_to_dashboard'])) {
      return parent::render($values);
    }
    $pid = $this->get_value($values, 'pid');

    $media_id = $this->get_value($values);
    return l($media_id, 'admin/maps-suite/dashboard/medias/' . $media_id . '/' . $pid);
  }

}
