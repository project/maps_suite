<?php

/**
 * @file
 * Definition of the Status field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

use Drupal\maps_import\Cache\Object\Profile as CacheProfile;

/**
 * Provides a custom views field.
 */
abstract class Field extends \views_handler_field {

  protected $configuration_type = false;

  public function option_definition() {
    $options = parent::option_definition();
    $options['use_human_name'] = ['default' => TRUE];
    return $options;
  }

  public function options_form(&$form, &$form_state) {
    $form['use_human_name'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use the label'),
      '#description' => t('Use the label instead of the raw code.'),
      '#default_value' => $this->options['use_human_name'],
    );
    parent::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if (empty($this->options['use_human_name'])) {
      return parent::render($values);
    }

    return false;
  }

  /**
   * Load the MaPS Suite profile.
   */
  protected function loadProfile($values) {
    $pid = $this->get_value($values, 'pid');
    return CacheProfile::getInstance()->loadSingle($pid);
  }

}
