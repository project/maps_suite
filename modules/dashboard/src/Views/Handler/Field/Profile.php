<?php

/**
 * @file
 * Definition of the Profile field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

use Drupal\maps_import\Cache\Object\Profile as CacheProfile;

/**
 * Provides a custom views field.
 */
class Profile extends Field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($return = parent::render($values)) {
      return $return;
    }

    $pid = $this->get_value($values);
    if (empty($pid)) {
      return FALSE;
    }

    $profile = CacheProfile::getInstance()->loadSingle($pid);

    return '<a href="/admin/maps-suite/profiles/'. $profile->getName() .'">' . $profile->getTitle() . '</a>';
  }
}
