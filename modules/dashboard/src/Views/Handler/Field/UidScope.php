<?php

/**
 * @file
 * Definition of the Profile field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

use Drupal\maps_import\Converter\ConverterInterface;

/**
 * Provides a custom views field.
 */
class UidScope extends Field {

  /**
   * {@inheritdoc}
   */
  public function render($values) {
    if ($return = parent::render($values)) {
      return $return;
    }

    $value = $this->get_value($values);

    // Scopes are hard-defined.
    $scopes = array(
      ConverterInterface::SCOPE_GLOBAL => t('Global'),
      ConverterInterface::SCOPE_PROFILE => t('Profile'),
    );

    return isset($scopes[$value]) ? $scopes[$value] : '';
  }
}


