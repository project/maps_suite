<?php

/**
 * @file
 * Definition of the Nature field handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Field;

/**
 * Provides a custom views field.
 */
class Nature extends Configuration {

  /**
   * {@inheritdoc}
   */
  protected $configuration_type = 'object_nature';

}
