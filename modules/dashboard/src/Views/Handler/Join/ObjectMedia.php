<?php

/**
 * @file
 * Definition of the Object2Entity join handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Join;

class ObjectMedia extends \views_join {

  function construct($table = NULL, $left_table = NULL, $left_field = NULL, $field = NULL, $extra = array(), $type = 'LEFT') {
    parent::construct($table, $left_table, $left_field, $field, $extra, $type);
  }

  function build_join($select_query, $table, $view_query) {
    if (empty($this->definition['table formula'])) {
      $right_table = $this->table;
    }
    else {
      $right_table = $this->definition['table formula'];
    }

    if ($this->left_table) {
      $left = $view_query->get_table_info($this->left_table);
      $left_field = "$left[alias].$this->left_field";
    }
    else {
      // This can be used if left_field is a formula or something. It should be used only *very* rarely.
      $left_field = $this->left_field;
    }

    $condition = "$left_field = $table[alias].$this->field";

    // Add the pid to the request.
    $condition .= " AND $left[alias].pid = $table[alias].pid";
    $select_query->addJoin($this->type, $right_table, $table['alias'], $condition);
  }

}
