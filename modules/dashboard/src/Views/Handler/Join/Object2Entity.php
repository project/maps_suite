<?php

/**
 * @file
 * Definition of the Object2Entity join handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Join;

class Object2Entity extends \views_join {

  function construct($table = NULL, $left_table = NULL, $left_field = NULL, $field = NULL, $extra = array(), $type = 'LEFT') {
    parent::construct($table, $left_table, $left_field, $field, $extra, $type);
  }

  function build_join($select_query, $table, $view_query) {
    $pivot_table = $this->definition['pivot_table'];

    if ($this->left_table == 'maps_import_entities') {
      // Add the first join with the pivot table.
      $alias = $this->left_table . '_' . $pivot_table;
      $condition = $this->left_table . '.id = ' . $alias . '.correspondence_id';
      $select_query->addJoin($this->type, $pivot_table, $alias, $condition);

      // Add the second join with the pivot table.
      $alias2 = $this->table;
      $condition = $alias2 . '.id = ' . $alias . '.maps_id AND ' . $alias2 . '.pid = ' . $this->left_table . '.pid';
      $select_query->addJoin($this->type, $this->table, $alias2, $condition);
    }
    else {
      // Add the first join with the pivot table.
      $alias = $this->left_table . '_' . $pivot_table;
      $condition = $this->left_table . '.id = ' . $alias . '.maps_id';
      $select_query->addJoin($this->type, $pivot_table, $alias, $condition);

      // Add the second join with the pivot table.
      $alias2 = $this->table;
      $condition = $alias2 . '.id = ' . $alias . '.correspondence_id AND ' . $alias2 . '.pid = ' . $this->left_table . '.pid';
      $select_query->addJoin($this->type, $this->table, $alias2, $condition);
    }
  }

}
