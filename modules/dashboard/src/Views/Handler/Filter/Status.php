<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

/**
 * Provides a custom views filter.
 */
class Status extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Status');

      $this->value_options = array(
        '8' => 'published',
        '4' => 'active',
        '2' => 'inactive',
        '1' => 'deleted',
      );
    }
  }
}
