<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

use Drupal\maps_import\Cache\Object\Converter as CacheConverter;

/**
 * Provides a custom views filter.
 */
class Converter extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Converters');

      // Load the converters from the MaPS Suite cache.
      $converters = CacheConverter::getInstance()->loadAll();

      $options = array();
      foreach($converters as $converter) {
        $options[$converter->getCid()] = $converter->getTitle() . ' (' . $converter->getCid() . ')';
      }

      asort($options);
      $this->value_options = $options;
    }
  }
}
