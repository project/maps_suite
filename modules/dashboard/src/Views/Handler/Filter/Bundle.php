<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

/**
 * Provides a custom views filter.
 */
class Bundle extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Bundle');

      $options = array();

      foreach (array_keys(entity_get_info()) as $machine_name) {
        foreach (field_info_bundles($machine_name) as $bundle_system_name => $bundle) {
          $options[$bundle_system_name] = $bundle['label'];
        }
      }

      asort($options);
      $this->value_options = $options;
    }
  }
}
