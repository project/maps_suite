<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

/**
 * Provides a custom views filter.
 */
class EntityType extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Bundle');

      $options = array();
      foreach (entity_get_info() as $machine_name => $entity) {
        $options[$machine_name] = $entity['label'];
      }

      asort($options);
      $this->value_options = $options;
    }
  }
}
