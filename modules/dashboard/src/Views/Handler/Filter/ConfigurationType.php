<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

use Drupal\maps_import\Fetcher\Configuration as ConfigurationFetcher;

/**
 * Provides a custom views filter.
 */
class ConfigurationType extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      return array(
        ConfigurationFetcher::ATTRIBUTE_OBJECT => t('Attribute object'),
        ConfigurationFetcher::CRITERIA_OBJECT => t('Criteria object'),
        ConfigurationFetcher::LINKED_OBJECT => t('Linked object'),
      );
    }
  }
}
