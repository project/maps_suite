<?php

/**
 * @file
 * Definition of the Profile filter handler.
 */

namespace Drupal\maps_dashboard\Views\Handler\Filter;

use Drupal\maps_import\Cache\Object\Profile as CacheProfile;

/**
 * Provides a custom views filter.
 */
class Profile extends \views_handler_filter_in_operator {

  /**
   * {@inheritdoc}
   */
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Profiles');

      // Load the profiles from the MaPS Suite cache.
      $profiles = CacheProfile::getInstance()->loadAll();

      $options = array();
      foreach($profiles as $profile) {
        $options[$profile->getPid()] = $profile->getTitle() . ' (' . $profile->getPid() . ')';
      }

      asort($options);
      $this->value_options = $options;
    }
  }
}
