<?php

/**
 * @file
 * Defines the MaPS Links Exception class.
 */

namespace Drupal\maps_links\Exception;

use Drupal\maps_suite\Exception\Exception;

/**
 * Exception class related to MaPS Link Mapping.
 */
class LinkException extends Exception {}
