<?php

namespace Drupal\maps_import\Tools\Object;

use Drupal\maps_import\Profile\Profile;
use Drupal\maps_import\Converter\ConverterInterface;

class MapsObjects {

  /**
   * Set all objects imported by a profile as inserted.
   *
   * @return int
   *   The number of objects that have been tagged as inserted.
   */
  public function setProfileObjectsAsInserted(Profile $profile) {
    $converters = maps_import_get_converters($profile->getPid());

    $total = 0;
    foreach ($converters as $converter) {
      $total += $this->setConverterObjectsAsInserted($converter);
    }

    return $total;
  }

  /**
   * Set all converted objects as inserted.
   *
   * @return int
   *   The number of objects that have been tagged as inserted.
   */
  public function setConverterObjectsAsInserted(ConverterInterface $converter) {
    // Get the right table names.
    $table = 'maps_import_' . strtolower($converter->getType()) . 's';
    $table_ids = 'maps_import_' . strtolower($converter->getType()) . '_ids';

    // Create the subquery that retrieve the object IDs that need to be updated.
    $subquery = db_select($table_ids, 'mioi');
    $subquery->join('maps_import_entities', 'mie', 'mie.id = mioi.correspondence_id');
    $subquery->addField('mioi', 'maps_id', 'id');
    $subquery->condition('mie.cid', $converter->getCid());

    // Perform the update.
    return db_update($table)
      ->fields(['inserted' => time()])
      ->condition ('id', $subquery, 'IN')
      ->condition ('inserted', 0, '=')
      ->execute();
  }

}