<?php

/**
 * @file
 * Defines the MaPS Import Filter Condition Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Filter Condition.
 */
class FilterConditionException extends Exception {}
