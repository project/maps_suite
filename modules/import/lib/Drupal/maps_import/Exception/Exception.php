<?php

/**
 * @file
 * Define the MaPS Import Exception generic class.
 */

namespace Drupal\maps_import\Exception;

use Drupal\maps_suite\Exception\Exception as MapsException;

/**
 * MaPS Import Exception class.
 */
class Exception extends MapsException {}
