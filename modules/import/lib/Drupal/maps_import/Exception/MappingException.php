<?php

/**
 * @file
 * Defines the MaPS Import Mapping Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Mapping.
 */
class MappingException extends Exception {}
