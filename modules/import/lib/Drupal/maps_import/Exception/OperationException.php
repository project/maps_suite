<?php

/**
 * @file
 * Define the MaPS Import Operation Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Operation.
 */
class OperationException extends Exception {}
