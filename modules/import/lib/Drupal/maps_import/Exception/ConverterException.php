<?php

/**
 * @file
 * Defines the MaPS Import Converter Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Converter.
 */
class ConverterException extends Exception {}
