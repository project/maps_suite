<?php

/**
 * @file
 * Defines the MaPS Import Web Service Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Web Service.
 */
class WebServiceException extends RequestException {}
