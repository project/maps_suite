<?php

/**
 * @file
 * Defines the MaPS Import Request Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Request.
 */
class RequestException extends Exception {}
