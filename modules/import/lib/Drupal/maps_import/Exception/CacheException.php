<?php

/**
 * @file
 * Defines the MaPS Import Cache Exception class.
 */

namespace Drupal\maps_import\Exception;

/**
 * Exception class related to MaPS Import Cache.
 */
class CacheException extends Exception {}
