<?php

/**
 * @file
 * Class that defines operation on MaPS Object's date attribute.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Attribute;

class Date extends Attribute {}
