<?php

/**
 * @file
 * Class that defines operation on MaPS Object's object-sound relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

class Sound extends Media {
  
  /**
   * @inheritdoc
   */
  protected $media_type = 4;
  
}
