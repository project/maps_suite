<?php

/**
 * @file
 * Class that defines operation on MaPS Object's default object-media relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

class DefaultMedia extends Media {

  /**
   * @inheritdoc
   */
  protected $media_type = 0;
  
}
