<?php

/**
 * @file
 * Class that defines operation on MaPS Object's object-media relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

use Drupal\maps_import\Converter\ConverterInterface;
use Drupal\maps_import\Mapping\Source\MapsSystem\EntityInterface;
use Drupal\maps_import\Mapping\Source\MapsSystem\PropertyWrapper;
use Drupal\maps_import\Mapping\Target\Drupal\EntityInterface as DrupalEntityInterface;
use Drupal\maps_import\Exception\MappingException;
use Drupal\maps_import\Profile\Profile;
use Drupal\maps_import\Converter\Media as MediaConverter;

abstract class Media extends PropertyWrapper {

  /**
   * Return the media key.
   *
   * @return string
   */
  public function getKey() {
    return 'media:' . $this->id;
  }

  /**
   * @inheritdoc
   */
  public function __construct(array $definition = array()) {
    parent::__construct($definition);
  }

    /**
   * Whether the property is translatable.
   *
   * @var boolean
   */
  protected $translatable = FALSE;

  /**
   * Whether the property may have multiple values.
   *
   * @var boolean
   */
  protected $multiple = TRUE;

  /**
   * @inheritdoc
   */
  public function extractValues(EntityInterface $entity, $options = array(), ConverterInterface $currentConverter) {
    //$medias = $entity->getMedias($this->media_type);
    $medias = $entity->getMedias(NULL);

    // Now, we have to filer the medias, to keep only the ones imported by
    // the given media converter.
    if (!$converter = maps_import_converter_load_by_name($this->id, $currentConverter->getProfile()->getPid())) {
      return array();
    }

    if (empty($medias)) {
      return array();
    }

    // For each media, we have to check if the media still pass the converter's
    // conditions.
    // Before, we used to create a query to get the medias imported by the
    // given converter. But it is not efficient, since it is possible that a
    // media that has been created by a given converter does not pass the
    // converter conditions anymore (for example, if an attribute's value
    // has changed since the media creation).
    foreach ($medias as $i => $media) {
      if (!$converter->getFilter()->checkConditions($media)) {
        unset($medias[$i]);
      }
    }

    if (!isset($options['media_start_range']) || !isset($options['media_limit_range'])) {
      throw new MappingException('Media ranges are not defined.', 0, array());
    }

    $values = array();
    $values[0] = array();

    $medias = array_values($medias);
    for ($i = ((int) $options['media_start_range'] - 1); $i < (int) $options['media_limit_range']; $i++) {
      if (isset($medias[$i])) {
        $values[DrupalEntityInterface::LANGUAGE_NONE][] = $medias[$i]['entity_id'];
      }
    }

    return $values;
  }

  /**
   * @inheritdoc
   */
  public function getGroupLabel() {
    return t('Object media relation');
  }

  /**
   * @inheritdoc
   */
  public function getTranslatedTitle() {
    return 'Media';
  }

  /**
   * Return the available Media classes.
   */
  static public function getMediaClassFromMediaType($mediaType) {
  	$mediaTypes = array(
  	  1 => 'Image',
  	  2 => 'Document',
  	  3 => 'Video',
  	  4 => 'Sound',
  	);

  	return 'Drupal\\maps_import\\Mapping\\Source\\MapsSystem\\Media\\' . $mediaTypes[$mediaType];
  }

  /**
   * Load a MaPS System® media entity from the MaPS media type id.
   *
   * @param $mediaType
   *   The MaPS System® media type id.
   * @param $definition
   *   The MaPS System® property definition.
   */
  static public function createMediaFromMediaType($mediaType, array $definition = array()) {
    $className = self::getMediaClassFromMediaType($mediaType);
    return new $className($definition);
  }

  /**
   * @inheritdoc
   */
  public function exists(Profile $profile) {
    return (bool) maps_import_converter_load_by_name($this->id, $profile->getPid());
  }

}
