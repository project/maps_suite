<?php
  
/**
 * @file
 * Object id property.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Property; 

class ObjectId extends Property {
  
	/**
   * @inheritdoc
   */	
	protected $typeCode = 'int'; 
	
}  
