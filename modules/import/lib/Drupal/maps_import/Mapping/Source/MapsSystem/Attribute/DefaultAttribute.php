<?php

/**
 * @file
 * Class that defines operation on MaPS Object's default attribute.
 * Works for:
 *    - html
 *    - text
 *    - string
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Attribute;

class DefaultAttribute extends Attribute {}
