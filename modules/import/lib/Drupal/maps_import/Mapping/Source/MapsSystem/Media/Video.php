<?php

/**
 * @file
 * Class that defines operation on MaPS Object's object-video relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

class Video extends Media {
  
  /**
   * @inheritdoc
   */
  protected $media_type = 3;
  
}
