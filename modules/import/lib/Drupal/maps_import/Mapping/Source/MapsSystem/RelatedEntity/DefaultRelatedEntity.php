<?php

/**
 * @file
 *
 * Define default MaPS System® related entity.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\RelatedEntity;

class DefaultRelatedEntity extends RelatedEntity {}
