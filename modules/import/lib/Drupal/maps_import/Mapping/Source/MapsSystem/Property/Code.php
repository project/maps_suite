<?php

/**
 * @file
 * Define the "code" MaPS System® property.
 */
namespace Drupal\maps_import\Mapping\Source\MapsSystem\Property;

class Code extends Property {

	/**
   * @inheritdoc
	 */
	protected $typeCode = 'string';

}
