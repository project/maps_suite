<?php

/**
 * @file
 * Class that defines operation on MaPS Object's object-image relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

class Image extends Media {
  
  /**
   * @inheritdoc
   */
  protected $media_type = 1;

}
