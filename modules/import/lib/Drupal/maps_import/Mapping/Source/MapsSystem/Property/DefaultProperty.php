<?php

/**
 * @file
 *
 * Define default MaPS System® property.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Property;

class DefaultProperty extends Property {}
