<?php

/**
 * @file
 * Class that defines operation on MaPS Object's object-document relation.
 */

namespace Drupal\maps_import\Mapping\Source\MapsSystem\Media;

class Document extends Media {
  
  /**
   * @inheritdoc
   */
  protected $media_type = 2;
  
}
