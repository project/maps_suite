<?php

namespace Drupal\maps_import\Mapping\Target\Drupal\Field;

/**
 * @file 
 * Custom management of the taxonomy term reference field.
 */

class TaxonomyTerm extends DefaultField {}
