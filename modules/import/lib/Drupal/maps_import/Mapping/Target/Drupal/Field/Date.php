<?php

namespace Drupal\maps_import\Mapping\Target\Drupal\Field;

/**
 * @file
 * Custom management of the date field.
 */

class Date extends DefaultField {

  /**
   * @inheritdoc
   */
  public function sanitize($values) {
    foreach ($values as &$value) {
      $datetime = new \DateTime($value);
      $value = $datetime->getTimestamp();
    }

    return $values;
  }

}