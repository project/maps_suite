README.txt
==========

DRUSH COMMANDS
==============

maps-import:

maps-import [pid] : Process all available operations for the profile pid.
maps-import [pid] --op=[operations] : Process defined operations (separated by commas) for the profile pid.
maps-import [pid] --list : List all available operations for the profile pid.

maps-profile:

maps-profile --list : List all available profiles.

maps-unlock: Delete the process lock.
